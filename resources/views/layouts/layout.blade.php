@include('layouts.header')
<div class="page-wrapper">
@yield('content')
</div>

@include('layouts.footer')

    @yield('script')

</body>

</html>
