<div style="margin-top: 200px;"></div>

<!--== popper -->
<script data-cfasync="false" src="{{secure_asset('cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script>
<script src="{{secure_asset('themes/base/frontassets/js/popper.min.js')}}"></script>

<!--== bootstrap -->
<script src="{{secure_asset('themes/base/frontassets/js/bootstrap.min.js')}}"></script>

<!--== appear -->
<script src="{{secure_asset('themes/base/frontassets/js/jquery.appear.js')}}"></script>

<!--== modernizr -->
<script src="{{secure_asset('themes/base/frontassets/js/modernizr.js')}}"></script>

<!--== magnific-popup -->
<script src="{{secure_asset('themes/base/frontassets/js/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

<!--=src="/js/rousel -->
<script src="{{secure_asset('themes/base/frontassets/js/owl-carousel/owl.carousel.min.js')}}"></script>

<!--== counter -->
<script src="{{secure_asset('themes/base/frontassets/js/counter/counter.js')}}"></script>

<!--== countdown -->
<script src="{{secure_asset('themes/base/frontassets/js/countdown/jquery.countdown.min.js')}}"></script>

<!--== isotope -->
<script src="{{secure_asset('themes/base/frontassets/js/isotope/isotope.pkgd.min.js')}}"></script>

<!--== mouse-parallax -->
<script src="{{secure_asset('themes/base/frontassets/js/mouse-parallax/tweenmax.min.js')}}"></script>
<script src="{{secure_asset('themes/base/frontassets/js/mouse-parallax/jquery-parallax.js')}}"></script>

<!--== contact-form -->
<script src="{{secure_asset('themes/base/frontassets/js/contact-form/contact-form.js')}}"></script>

<!--== validate -->
<script src="{{secure_asset('themes/base/frontassets/js/contact-form/jquery.validate.min.js')}}"></script>

<!--== map api -->
<script src="https://maps.googleapis.com/maps/api/js"></script>

<!--== map -->
<script src="{{secure_asset('themes/base/frontassets/js/map.js')}}"></script>

<!--== wow -->
<script src="{{secure_asset('themes/base/frontassets/js/wow.min.js')}}"></script>

<!--== theme-script -->
<script src="{{secure_asset('themes/base/frontassets/js/theme-script.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="{{secure_asset('assets/a628c5a8/yii.validation.js')}}"></script>
<script src="{{secure_asset('assets/a628c5a8/yii.activeForm.js')}}"></script>
<script>jQuery(function ($) {
        jQuery('#login-form').yiiActiveForm([{
            "id": "loginform-username",
            "name": "username",
            "container": ".field-loginform-username",
            "input": "#loginform-username",
            "error": ".invalid-feedback",
            "validate": function (attribute, value, messages, deferred, $form) {
                yii.validation.required(value, messages, {"message": "Username cannot be blank."});
            }
        }, {
            "id": "loginform-password",
            "name": "password",
            "container": ".field-loginform-password",
            "input": "#loginform-password",
            "error": ".invalid-feedback",
            "validate": function (attribute, value, messages, deferred, $form) {
                yii.validation.required(value, messages, {"message": "Password cannot be blank."});
            }
        }, {
            "id": "loginform-rememberme",
            "name": "rememberMe",
            "container": ".field-loginform-rememberme",
            "input": "#loginform-rememberme",
            "error": ".invalid-feedback",
            "validate": function (attribute, value, messages, deferred, $form) {
                yii.validation.boolean(value, messages, {
                    "trueValue": "1",
                    "falseValue": "0",
                    "message": "Remember Me must be either \"1\" or \"0\".",
                    "skipOnEmpty": 1
                });
            }
        }], {
            "errorSummary": ".error-summary.alert.alert-danger",
            "errorCssClass": "is-invalid",
            "successCssClass": "is-valid",
            "validationStateOn": "input"
        });
    });</script>


