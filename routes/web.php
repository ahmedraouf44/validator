<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/modify', function () {
    return view('website.modify');
});
Route::post('/modifyConfirm', 'ReservationController@modify')->name('modify');

Route::get('/ReservationConfirm', 'ReservationController@validateReservation')->name('validateReservation');
